# SET UP A READY TO USE VM USING VAGRANT AND ANSIBLE

## Set up

1. Clone the repository using: `git clone --recurse-submodules git@gitlab.com:meli-pisarello/workstation-setup.git`.
2. Install a VM provider like Virtual Box.
3. Install Vagrant.
4. if you need to use your own ssh keys, copy your .ssh folder content to the project's ssh folder,
 else change the value of the ssh_gen variable to true in the VagrantFile.
5. Run `vagrant up` in the project's root folder to create the VM.
6. Connect to the VM and clone your repo.
7. Enjoy!

---

## Basic usage

* Starts and provisions the vagrant environment: `vagrant up`
* Stop the VM: `vagrant halt`
* Stops and removes all trace of the VM: `vagrant destroy`
* Connects the VM via ssh: `vagrant ssh`
* Get the VM ssh config: `vagrant ssh-config`

---

## Working remotely

### Visual Studio Code

1. Run `vagrant ssh-config` in the project's root folder and copy the output to your ssh config file.
2. Install the `Remote - SSH` extension in your vscode.
3. Go to `View > Command Palette` and search for `remote-ssh: connect to host`.
4. Select the vagrant host added to your config, by default its `default`.
5. When the connection is successful, you can open any folder from your VM and start working.

### PHPTormenta

1. Install [JetBrains Gateway](https://www.jetbrains.com/help/idea/remote-development-a.html).
2. Select `Connect via SSH`.
3. Click on the gear icon to add a new connection.
4. Run `vagrant ssh-config` in the project's root folder and copy the output to the connection's settings (for the authentication type choose `Key pair`).
5. Save the new connection and click on `Check Connection and Continue`.
6. Select `PhpStorm` on the IDE version and the folder you will be working on.
7. You will be prompted to enter a licence key once the IDE is installed on the JetBrains Client.
